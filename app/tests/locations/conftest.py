import pytest

from locations.models import Country, LevelOne, LevelTwo, LevelThree, LevelFour


@pytest.fixture(scope='function')
def add_country():
    def _add_country(code, name, official_name):
        country = Country.objects.create(code=code, name=name, official_name=official_name)
        return country
    return _add_country

@pytest.fixture(scope='function')
def add_levelone():
    def _add_levelone(kind, name, country):
        levelone = LevelOne.objects.create(kind=kind, name=name, country=country)
        return levelone
    return _add_levelone

@pytest.fixture(scope='function')
def add_leveltwo():
    def _add_leveltwo(kind, name, levelone):
        leveltwo = LevelTwo.objects.create(kind=kind, name=name, levelone=levelone)
        return leveltwo
    return _add_leveltwo

@pytest.fixture(scope='function')
def add_levelthree():
    def _add_levelthree(kind, name, leveltwo):
        levelthree = LevelThree.objects.create(kind=kind, name=name, leveltwo=leveltwo)
        return levelthree
    return _add_levelthree

@pytest.fixture(scope='function')
def add_levelfour():
    def _add_levelfour(kind, name, leveltwo, levelthree):
        levelfour = LevelFour.objects.create(kind=kind, name=name, leveltwo=leveltwo, levelthree=levelthree)
        return levelfour
    return _add_levelfour
