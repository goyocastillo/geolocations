import pytest

from locations.models import Country, LevelOne, LevelTwo, LevelThree, LevelFour

@pytest.mark.django_db
def test_country_model():
    country = Country(code='TC', name='TestCountry', official_name='Official Test Country')
    country.save()
    assert country.code == 'TC'
    assert country.name == 'TestCountry'
    assert country.official_name == 'Official Test Country'

@pytest.mark.django_db
def test_levelone_model():
    country = Country(code='TC', name='TestCountry', official_name='Official Test Country')
    country.save()
    levelone = LevelOne(kind='TestType', name='TestLevelOne', country=country)
    assert levelone.kind == 'TestType'
    assert levelone.name == 'TestLevelOne'
    assert levelone.country.id == country.id
    

@pytest.mark.django_db
def test_leveltwo_model():
    country = Country(code='TC', name='TestCountry', official_name='Official Test Country')
    country.save()
    levelone = LevelOne(kind='TestType', name='TestLevelOne', country=country)
    levelone.save()
    leveltwo = LevelTwo(kind='TestType2', name='TestLevelTwo', levelone=levelone)
    assert leveltwo.kind == 'TestType2'
    assert leveltwo.name == 'TestLevelTwo'
    assert leveltwo.levelone.id == levelone.id
    

@pytest.mark.django_db
def test_levelthree_model():
    country = Country(code='TC', name='TestCountry', official_name='Official Test Country')
    country.save()
    levelone = LevelOne(kind='TestType', name='TestLevelOne', country=country)
    levelone.save()
    leveltwo = LevelTwo(kind='TestType2', name='TestLevelTwo', levelone=levelone)
    leveltwo.save()
    levelthree = LevelThree(kind='TestType3', name='TestLevelThree',leveltwo=leveltwo)
    assert levelthree.kind == 'TestType3'
    assert levelthree.name == 'TestLevelThree'
    assert levelthree.leveltwo.id == leveltwo.id

@pytest.mark.django_db
def test_levelfour_model_with_levelthree():
    country = Country(code='TC', name='TestCountry', official_name='Official Test Country')
    country.save()
    levelone = LevelOne(kind='TestType', name='TestLevelOne', country=country)
    levelone.save()
    leveltwo = LevelTwo(kind='TestType2', name='TestLevelTwo', levelone=levelone)
    leveltwo.save()
    levelthree = LevelThree(kind='TestType3', name='TestLevelThree',leveltwo=leveltwo)
    levelthree.save()
    levelfour = LevelFour(kind='TestType4', name='TestLevelFour',levelthree=levelthree)
    assert levelfour.kind == 'TestType4'
    assert levelfour.name == 'TestLevelFour'
    assert levelfour.levelthree.id == levelthree.id

@pytest.mark.django_db
def test_levelfour_model_without_levelthree():
    country = Country(code='TC', name='TestCountry', official_name='Official Test Country')
    country.save()
    levelone = LevelOne(kind='TestType', name='TestLevelOne', country=country)
    levelone.save()
    leveltwo = LevelTwo(kind='TestType2', name='TestLevelTwo', levelone=levelone)
    leveltwo.save()
    levelfour = LevelFour(kind='TestType4wo3', name='TestLevelFourWO3',leveltwo=leveltwo)
    assert levelfour.kind == 'TestType4wo3'
    assert levelfour.name == 'TestLevelFourWO3'
    assert levelfour.leveltwo.id == leveltwo.id