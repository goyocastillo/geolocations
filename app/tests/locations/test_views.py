import json

import pytest
from django.utils.http import urlencode
from django.urls import reverse

def reverse_querystring(view, urlconf=None, args=None, kwargs=None, current_app=None, query_kwargs=None):
    '''Custom reverse to handle query strings.
    Usage:
        reverse('app.views.my_view', kwargs={'pk': 123}, query_kwargs={'search': 'Bob'})
    '''
    base_url = reverse(view, urlconf=urlconf, args=args, kwargs=kwargs, current_app=current_app)
    if query_kwargs:
        return '{}?{}'.format(base_url, urlencode(query_kwargs))
    return base_url


class TestCountryView:
    @pytest.mark.django_db
    def test_get_single_country(self,client, add_country):
        country = add_country(code="TC", name="TestCountry", official_name="Official Test Country")
        resp = client.get(f'/locations/api/countries/{country.id}/')
        assert resp.status_code == 200
        assert resp.data['code'] == 'TC'

    def test_get_single_country_incorrect(self, client):
        resp = client.get(f'/locations/api/countries/foo/')
        assert resp.status_code == 404


    @pytest.mark.django_db
    def test_get_all_countries(self, client, add_country):
        country_one = add_country(code="T1", name="TestCountry1", official_name="Official Test Country1")
        country_two = add_country(code="T2", name="TestCountry2", official_name="Official Test Country2")
        resp = client.get(f"/locations/api/countries/")
        assert resp.status_code == 200
        assert resp.data[0]["code"] == country_one.code
        assert resp.data[1]["code"] == country_two.code

class TestLevelOneView:
    @pytest.mark.django_db
    def test_get_all_levelOne_by_country(self, client, add_country, add_levelone):
        country = add_country(code="T1", name="TestCountry1", official_name="Official Test Country1")
        levelone_one = add_levelone(kind="TypeTest1", name="TestLevelOne1", country=country)
        levelone_two = add_levelone(kind="TypeTest2", name="TestLevelOne2", country=country)
        resp = client.get(f"/locations/api/countries/{country.code}/levelones/")
        assert resp.status_code == 200
        assert resp.data[0]["name"] == levelone_one.name
        assert resp.data[1]["name"] == levelone_two.name

    @pytest.mark.django_db
    def test_get_single_levelOne(self,client, add_country, add_levelone):
        country = add_country(code="T1", name="TestCountry1", official_name="Official Test Country1")
        levelone = add_levelone(kind="TypeTest", name="TestLevelOne", country=country)
        resp = client.get(f'/locations/api/levelones/{levelone.id}/')
        assert resp.status_code == 200
        assert resp.data["name"] == levelone.name
    
    @pytest.mark.django_db
    def test_levelOne_name_filter(self, client, add_country, add_levelone):
        country = add_country(code="T1", name="TestCountry1", official_name="Official Test Country1")
        for i in range(5):
            add_levelone(kind="TypeTest{}".format(i), name="TestLevelOne{}".format(i), country=country)
        path = reverse_querystring('levelone-list', kwargs={'code': country.code}, query_kwargs={'name': 'TestLevelOne0'})
        resp = client.get(path) 
        assert resp.status_code == 200
        assert len(resp.data) == 1
        assert resp.data[0]["name"] == 'TestLevelOne0'


class TestLevelTwoView:
    @pytest.mark.django_db
    def test_get_all_levelTwo_by_levelOne(self, client, add_country, add_levelone, add_leveltwo):
        country = add_country(code="T1", name="TestCountry1", official_name="Official Test Country1")
        levelone = add_levelone(kind="TypeTest1", name="TestLevelOne1", country=country)
        leveltwo_one = add_leveltwo(kind="TypeTest21", name="TestLevelTwo1", levelone=levelone)
        leveltwo_two = add_leveltwo(kind="TypeTest22", name="TestLevelTwo2", levelone=levelone)
        resp = client.get(f"/locations/api/levelones/{levelone.id}/leveltwos/")
        assert resp.status_code == 200
        assert resp.data[0]["name"] == leveltwo_one.name
        assert resp.data[1]["name"] == leveltwo_two.name


    @pytest.mark.django_db
    def test_get_single_levelTwo(self,client, add_country, add_levelone, add_leveltwo):
        country = add_country(code="T1", name="TestCountry1", official_name="Official Test Country1")
        levelone = add_levelone(kind="TypeTest", name="TestLevelOne", country=country)
        leveltwo = add_leveltwo(kind="TypeTest2", name="TestLevelTwo", levelone=levelone)
        resp = client.get(f'/locations/api/leveltwos/{leveltwo.id}/')
        assert resp.status_code == 200
        assert resp.data["name"] == leveltwo.name


class TestLevelThreeView:
    @pytest.mark.django_db
    def test_get_all_levelThree_by_levelTwo(self, client, add_country, add_levelone, add_leveltwo, add_levelthree):
        country = add_country(code="T1", name="TestCountry1", official_name="Official Test Country1")
        levelone = add_levelone(kind="TypeTest1", name="TestLevelOne", country=country)
        leveltwo = add_leveltwo(kind="TypeTest2", name="TestLevelTwo", levelone=levelone)
        levelthree_one = add_levelthree(kind="TypeTest31", name="TestLevelThree1", leveltwo=leveltwo)
        levelthree_two = add_levelthree(kind="TypeTest32", name="TestLevelThree2", leveltwo=leveltwo)
        resp = client.get(f"/locations/api/leveltwos/{leveltwo.id}/levelthrees/")
        assert resp.status_code == 200
        assert resp.data[0]["name"] == levelthree_one.name
        assert resp.data[1]["name"] == levelthree_two.name

    @pytest.mark.django_db
    def test_get_single_levelThree(self,client, add_country, add_levelone, add_leveltwo, add_levelthree):
        country = add_country(code="T1", name="TestCountry1", official_name="Official Test Country1")
        levelone = add_levelone(kind="TypeTest", name="TestLevelOne", country=country)
        leveltwo = add_leveltwo(kind="TypeTest2", name="TestLevelTwo", levelone=levelone)
        levelthree = add_levelthree(kind="TypeTest3", name="TestLevelThree", leveltwo=leveltwo)
        resp = client.get(f'/locations/api/levelthrees/{levelthree.id}/')
        assert resp.status_code == 200
        assert resp.data["name"] == levelthree.name

class TestLevelFourView:
    @pytest.mark.django_db
    def test_get_all_levelFour_by_levelThree(self, client, add_country, add_levelone, 
                                    add_leveltwo, add_levelthree, add_levelfour):
        country = add_country(code="T1", name="TestCountry1", official_name="Official Test Country1")
        levelone = add_levelone(kind="TypeTest1", name="TestLevelOne", country=country)
        leveltwo = add_leveltwo(kind="TypeTest2", name="TestLevelTwo", levelone=levelone)
        levelthree = add_levelthree(kind="TypeTest3", name="TestLevelThree", leveltwo=leveltwo)
        levelfour_one = add_levelfour(kind="TypeTest41", name="TestLevelFour1", leveltwo=leveltwo, levelthree=levelthree)
        levelfour_two = add_levelfour(kind="TypeTest42", name="TestLevelFour2", leveltwo=leveltwo, levelthree=levelthree)
        resp = client.get(f"/locations/api/levelthrees/{levelthree.id}/levelfours/")
        assert resp.status_code == 200
        assert resp.data[0]["name"] == levelfour_one.name
        assert resp.data[1]["name"] == levelfour_two.name

    @pytest.mark.django_db
    def test_get_single_levelFour(self,client, add_country, add_levelone, add_leveltwo, add_levelthree, add_levelfour):
        country = add_country(code="T1", name="TestCountry1", official_name="Official Test Country1")
        levelone = add_levelone(kind="TypeTest", name="TestLevelOne", country=country)
        leveltwo = add_leveltwo(kind="TypeTest2", name="TestLevelTwo", levelone=levelone)
        levelthree = add_levelthree(kind="TypeTest3", name="TestLevelThree", leveltwo=leveltwo)
        levelfour = add_levelfour(kind="TypeTest4", name="TestLevelFour", leveltwo=leveltwo, levelthree=levelthree)
        resp = client.get(f'/locations/api/levelfours/{levelfour.id}/')
        assert resp.status_code == 200
        assert resp.data["name"] == levelfour.name