import pytest

from locations.api.serializers import (
            CountrySerializer, 
            LevelOneSerializer, 
            LevelTwoSerializer, 
            LevelThreeSerializer,
            LevelFourSerializer,)


class TestCountrySerializer:
    def test_valid_country_serializer(self):
        valid_serializer_data = {
            "code": "VE",
            "name": "Venezuela",
            "official_name": "República Bolivariana de Venezuela"
        }
        serializer = CountrySerializer(data=valid_serializer_data)
        assert serializer.is_valid()
        assert serializer.validated_data == valid_serializer_data
        assert serializer.data == valid_serializer_data
        assert serializer.errors == {}

    def test_invalid_country_serializer(self):
        invalid_serializer_data = {
            "name": "Venezuela",
            "official_name": "República Bolivariana de Venezuela"
        }
        serializer = CountrySerializer(data=invalid_serializer_data)
        assert not serializer.is_valid()
        assert serializer.validated_data == {}
        assert serializer.data == invalid_serializer_data
        assert serializer.errors == {"code": ["This field is required."]}

class TestLevelOneSerializer:

    @pytest.mark.django_db
    def test_valid_levelOne_serializer(self, add_country, add_levelone):
        country = add_country(code="TC", name="TestCountry", official_name="OfficialTestCountry")
        levelone = add_levelone(kind="LevelOne Type", name='TestLevelOne', country=country)
        valid_serializer_data = {
            'id': levelone.id,
            'kind': levelone.kind, 
            'name': levelone.name,
            'country': {
                "code": country.code, 
                "name": country.name,
                "official_name": country.official_name
            }
        }
        serializer = LevelOneSerializer(data=valid_serializer_data)
        assert serializer.is_valid(raise_exception=True)
        assert serializer.validated_data == valid_serializer_data
        assert serializer.data == valid_serializer_data
        assert serializer.errors == {}

    @pytest.mark.django_db
    def test_invalid_levelOne_serializer(self, add_country, add_levelone):
        country = add_country(code="TC", name="TestCountry", official_name="OfficialTestCountry")
        levelone = add_levelone(kind="LevelOne Type", name='TestLevelOne', country=country)
        invalid_serializer_data = {
            'id': levelone.id,
            'kind': levelone.kind, 
            'name': levelone.name,
        }
        serializer = LevelOneSerializer(data=invalid_serializer_data)
        assert not serializer.is_valid()
        assert serializer.validated_data == {}
        assert serializer.data == invalid_serializer_data
        assert serializer.errors == {"country": ["This field is required."]}


class TestLevelTwoSerializer:
    
    @pytest.mark.django_db
    def test_valid_levelTwo_serializer(self, add_country, add_levelone, add_leveltwo):
        country = add_country(code="TC", name="TestCountry", official_name="OfficialTestCountry")
        levelone = add_levelone(kind="LevelOne Type", name='TestLevelOne', country=country)
        leveltwo = add_leveltwo(kind="LevelTwo Type", name='TestLevelTwo', levelone=levelone)
        valid_serializer_data = {
            'id': leveltwo.id,            'kind': 'Test Type Two', 
            'name': leveltwo.name,
            'levelone': {
                'id': levelone.id,
                'kind': levelone.kind, 
                'name': levelone.name,
                'country': {
                    'code': country.code,
                    'name': country.name,
                    'official_name': country.official_name
                }
            }
        }
        serializer = LevelTwoSerializer(data=valid_serializer_data)
        assert serializer.is_valid(raise_exception=True)
        assert serializer.validated_data == valid_serializer_data
        assert serializer.data == valid_serializer_data
        assert serializer.errors == {}

    @pytest.mark.django_db
    def test_invalid_levelTwo_serializer(self, add_country, add_levelone, add_leveltwo):
        country = add_country(code="TC", name="TestCountry", official_name="OfficialTestCountry")
        levelone = add_levelone(kind="LevelOne Type", name='TestLevelOne', country=country)
        leveltwo = add_leveltwo(kind="LevelTwo Type", name='TestLevelTwo', levelone=levelone)
        invalid_serializer_data = {
            'id': leveltwo.id,
            'kind': leveltwo.kind, 
            'name': leveltwo.name,
        }
        serializer = LevelTwoSerializer(data=invalid_serializer_data)
        assert not serializer.is_valid()
        assert serializer.validated_data == {}
        assert serializer.data == invalid_serializer_data
        assert serializer.errors == {"levelone": ["This field is required."]}

class TestLevelThreeSerializer:

    @pytest.mark.django_db
    def test_valid_levelThree_serializer(self, add_country, add_levelone, add_leveltwo, add_levelthree):
        country = add_country(code="TC", name="TestCountry", official_name="OfficialTestCountry")
        levelone = add_levelone(kind="LevelOne Type", name='TestLevelOne', country=country)
        leveltwo = add_leveltwo(kind="LevelTwo Type", name='TestLevelTwo', levelone=levelone)
        levelthree = add_levelthree(kind="LevelThree Type", name='TestLevelThree', leveltwo=leveltwo)
        valid_serializer_data = {
            'id': levelthree.id,
            'kind': levelthree.kind, 
            'name': levelthree.name,
            'leveltwo': {
                'id': leveltwo.id,
                'kind': leveltwo.kind, 
                'name': leveltwo.name,
                'levelone': {
                    'id': levelone.id,
                    'kind': levelone.kind, 
                    'name': levelone.name,
                    'country': {
                        'code': country.code,
                        'name': country.name,
                        'official_name': country.official_name
                    }
                }
            }
        }
        serializer = LevelThreeSerializer(data=valid_serializer_data)
        assert serializer.is_valid(raise_exception=True)
        assert serializer.validated_data == valid_serializer_data
        assert serializer.data == valid_serializer_data
        assert serializer.errors == {}

    @pytest.mark.django_db
    def test_invalid_levelThree_serializer(self, add_country, add_levelone, add_leveltwo, add_levelthree):
        country = add_country(code="TC", name="TestCountry", official_name="OfficialTestCountry")
        levelone = add_levelone(kind="LevelOne Type", name='TestLevelOne', country=country)
        leveltwo = add_leveltwo(kind="LevelTwo Type", name='TestLevelTwo', levelone=levelone)
        levelthree = add_levelthree(kind="LevelThree Type", name='TestLevelThree', leveltwo=leveltwo)
        invalid_serializer_data = {
            'id': levelthree.id,
            'kind': levelthree.kind, 
            'name': levelthree.name,
        }
        serializer = LevelThreeSerializer(data=invalid_serializer_data)
        assert not serializer.is_valid()
        assert serializer.validated_data == {}
        assert serializer.data == invalid_serializer_data
        assert serializer.errors == {"leveltwo": ["This field is required."]}


class TestLevelFourSerializer:

    @pytest.mark.django_db
    def test_valid_levelFour_with_l3_serializer(self, add_country, add_levelone, add_leveltwo, add_levelthree, add_levelfour):
        country = add_country(code="TC", name="TestCountry", official_name="OfficialTestCountry")
        levelone = add_levelone(kind="LevelOne Type", name='TestLevelOne', country=country)
        leveltwo = add_leveltwo(kind="LevelTwo Type", name='TestLevelTwo', levelone=levelone)
        levelthree = add_levelthree(kind="LevelThree Type", name='TestLevelThree', leveltwo=leveltwo)
        levelfour = add_levelfour(kind='LevelFour Type', name='TestLevelFour', levelthree=levelthree, leveltwo=leveltwo)
        valid_serializer_data = {
            'id': levelfour.id,
            'kind': levelfour.kind, 
            'name': levelfour.name,
            'levelthree': {
                'id': levelthree.id,
                'kind': levelthree.kind, 
                'name': levelthree.name,
                'leveltwo': {
                    'id': leveltwo.id,
                    'kind': leveltwo.kind, 
                    'name': leveltwo.name,
                    'levelone': {
                        'id': levelone.id,
                        'kind': levelone.kind, 
                        'name': levelone.name,
                        'country': {
                            'code': country.code,
                            'name': country.name,
                            'official_name': country.official_name
                        }
                    }
                }
            }
        }
        serializer = LevelFourSerializer(data=valid_serializer_data)
        assert serializer.is_valid(raise_exception=True)
        assert serializer.validated_data == valid_serializer_data
        assert serializer.data == valid_serializer_data
        assert serializer.errors == {}

    @pytest.mark.django_db
    def test_valid_levelFour_without_l3_serializer(self, add_country, add_levelone, add_leveltwo, add_levelthree, add_levelfour):
        country = add_country(code="TC", name="TestCountry", official_name="OfficialTestCountry")
        levelone = add_levelone(kind="LevelOne Type", name='TestLevelOne', country=country)
        leveltwo = add_leveltwo(kind="LevelTwo Type", name='TestLevelTwo', levelone=levelone)
        levelthree = add_levelthree(kind="LevelThree Type", name='TestLevelThree', leveltwo=leveltwo)
        levelfour = add_levelfour(kind='LevelFour Type', name='TestLevelFour', levelthree=levelthree, leveltwo=leveltwo)
        valid_serializer_data = {
            'id': levelfour.id,
            'kind': levelfour.kind, 
            'name': levelfour.name,
            'levelthree': {
                'id': levelthree.id,
                'kind': levelthree.kind, 
                'name': levelthree.name,
                'leveltwo': {
                    'id': leveltwo.id,
                    'kind': leveltwo.kind, 
                    'name': leveltwo.name,
                    'levelone': {
                        'id': levelone.id,
                        'kind': levelone.kind, 
                        'name': levelone.name,
                        'country': {
                            'code': country.code,
                            'name': country.name,
                            'official_name': country.official_name
                        }
                    }
                }
            }
        }
        serializer = LevelFourSerializer(data=valid_serializer_data)
        assert serializer.is_valid(raise_exception=True)
        assert serializer.validated_data == valid_serializer_data
        assert serializer.data == valid_serializer_data
        assert serializer.errors == {}

    @pytest.mark.django_db
    def test_invalid_levelFour_serializer(self, add_country, add_levelone, add_leveltwo, add_levelthree, add_levelfour):
        country = add_country(code="TC", name="TestCountry", official_name="OfficialTestCountry")
        levelone = add_levelone(kind="LevelOne Type", name='TestLevelOne', country=country)
        leveltwo = add_leveltwo(kind="LevelTwo Type", name='TestLevelTwo', levelone=levelone)
        levelthree = add_levelthree(kind="LevelThree Type", name='TestLevelThree', leveltwo=leveltwo)
        levelfour = add_levelfour(kind='LevelFour Type', name='TestLevelFour', levelthree=levelthree, leveltwo=leveltwo)
        invalid_serializer_data = {
            'kind': levelfour.kind, 
            'name': levelfour.name,
        }
        serializer = LevelFourSerializer(data=invalid_serializer_data)
        assert not serializer.is_valid()
        assert serializer.validated_data == {}
        assert serializer.data == invalid_serializer_data
        assert serializer.errors == {"id": ["This field is required."]}

    # @pytest.mark.django_db
    # def test_invalid_levelFour_with_level_serializer(self, add_country, add_levelone, add_leveltwo, add_levelthree, add_levelfour):
    #     country = add_country(code="TC", name="TestCountry", official_name="OfficialTestCountry")
    #     levelone = add_levelone(kind="LevelOne Type", name='TestLevelOne', country=country)
    #     leveltwo = add_leveltwo(kind="LevelTwo Type", name='TestLevelTwo', levelone=levelone)
    #     levelthree = add_levelthree(kind="LevelThree Type", name='TestLevelThree', leveltwo=leveltwo)
    #     levelfour = add_levelfour(kind='LevelFour Type', name='TestLevelFour', levelthree=levelthree, leveltwo=leveltwo)
    #     invalid_serializer_data = {
    #         'id': levelfour.id,
    #         'kind': levelfour.kind, 
    #         'name': levelfour.name,
    #         'levelthree': {
    #             'id': levelthree.id,
    #             'kind': levelthree.kind, 
    #             'name': levelthree.name,
    #             'leveltwo': {
    #                 'id': leveltwo.id,
    #                 'kind': leveltwo.kind, 
    #                 'name': leveltwo.name,
    #                 'levelone': {
    #                     'id': levelone.id,
    #                     'kind': levelone.kind, 
    #                     'name': levelone.name,
    #                     'country': {
    #                         'code': country.code,
    #                         'name': country.name,
    #                         'official_name': country.official_name
    #                     }
    #                 }
    #             }
    #         }
    #     }
    #     serializer = LevelFourSerializer(data=invalid_serializer_data)
    #     assert not serializer.is_valid()
    #     assert serializer.validated_data == {}
    #     assert serializer.data == invalid_serializer_data
    #     assert serializer.errors == {"leveltwo": ["This field is required."]}