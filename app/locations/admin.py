from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DefaultUserAdmin

from .models import Country, CustomUser


@admin.register(CustomUser)
class UserAdmin(DefaultUserAdmin):
    pass


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    fields = (
        'code', 'name', 'official_name',
    )
    list_display = (
        'code', 'name', 'official_name',
    )
