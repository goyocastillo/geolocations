import json
from django.core.management.base import BaseCommand

from locations.models import (Country, LevelOne, LevelTwo, LevelThree, LevelFour)

class Command(BaseCommand):

    def handle(self, *args, **options):

        print('init VE data loading...')

        LEVEL_ONE_TYPE = 'Estado'
        LEVEL_TWO_TYPE = 'Municipio'
        LEVEL_THREE_TYPE = 'Ciudad'
        LEVEL_FOUR_TYPE = 'Parroquia'


        data_vzla_json = open("../app/locations/files/vzla_data.json")
        data_vzla = json.load(data_vzla_json)

        country= Country(code='VE', name='Venezuela', official_name='República Bolivariana de Venezuela')
        country.save()
        provincia = ''
        municipio = ''
        ciudad = ''
        for dict in data_vzla:

            if provincia != dict[LEVEL_ONE_TYPE]:
                provincia = dict[LEVEL_ONE_TYPE]    
                levelone = LevelOne(kind=LEVEL_ONE_TYPE, name=provincia, country=country)
                levelone.save()

            if municipio != dict[LEVEL_TWO_TYPE]:
                municipio = dict[LEVEL_TWO_TYPE]
                leveltwo = LevelTwo(kind=LEVEL_TWO_TYPE, name=municipio, levelone=levelone)
                leveltwo.save()

            if ciudad != dict[LEVEL_THREE_TYPE]:
                ciudad = dict[LEVEL_THREE_TYPE]
                levelthree = LevelThree(kind=LEVEL_THREE_TYPE, name=ciudad, leveltwo=leveltwo)
                levelthree.save()

            parroquia = dict[LEVEL_FOUR_TYPE]
            levelfour = LevelFour(kind=LEVEL_FOUR_TYPE,name=parroquia, leveltwo=leveltwo, levelthree=levelthree)
            levelfour.save()

        print('data loaded')