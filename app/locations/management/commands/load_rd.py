import json
from django.core.management.base import BaseCommand

from locations.models import (Country, LevelOne, LevelTwo, LevelThree)

class Command(BaseCommand):

    def handle(self, *args, **options):

        print('init data loading...')

        LEVEL_ONE_TYPE = 'Provincia'
        LEVEL_TWO_TYPE = 'Municipio'
        LEVEL_THREE_TYPE = 'Ciudad'

        data_rd_json = open("../app/locations/files/rd_data.json")
        data_rd = json.load(data_rd_json)

        country = Country(code='DO', name='República Dominicana', official_name='República Dominicana')
        country.save()
        provincia = ''
        municipio = ''
        ciudad = ''
        for dict in data_rd:

            if provincia != dict[LEVEL_ONE_TYPE]:
                provincia = dict[LEVEL_ONE_TYPE]    
                levelone = LevelOne(kind=LEVEL_ONE_TYPE, name=provincia, country=country)
                levelone.save()

            if municipio != dict[LEVEL_TWO_TYPE]:
                municipio = dict[LEVEL_TWO_TYPE]
                leveltwo = LevelTwo(kind=LEVEL_TWO_TYPE, name=municipio, levelone=levelone)
                leveltwo.save()

            ciudad = dict[LEVEL_THREE_TYPE]
            levelthree = LevelThree(kind=LEVEL_THREE_TYPE,name=ciudad, leveltwo=leveltwo)
            levelthree.save()

        print('data loaded')