import json
from django.core.management.base import BaseCommand

from locations.models import (Country, LevelOne, LevelTwo, LevelThree, LevelFour)


class Command(BaseCommand):

    def _rd_data(self):
        levels = {'LEVEL_ONE_TYPE': 'Provincia',
                  'LEVEL_TWO_TYPE': 'Municipio',
                  'LEVEL_THREE_TYPE': 'Ciudad'}

        data = open("../app/locations/files/rd_data.json")
        country = Country(code='DO', name='República Dominicana', official_name='República Dominicana')
        country.save()
        return country, data, levels

    def _ve_data(self):
        levels = {'LEVEL_ONE_TYPE': 'Estado',
                  'LEVEL_TWO_TYPE': 'Municipio',
                  'LEVEL_THREE_TYPE': 'Ciudad',
                  'LEVEL_FOUR_TYPE': 'Parroquia'}

        data = open("../app/locations/files/vzla_data.json")
        country = Country(code='VE', name='Venezuela', official_name='República Bolivariana de Venezuela')
        country.save()
        return country, data, levels

    def _mx_data(self):
        levels = {'LEVEL_ONE_TYPE': 'Estado',
                  'LEVEL_TWO_TYPE': 'Municipio',
                  'LEVEL_THREE_TYPE': 'Ciudad',}

        data = open("../app/locations/files/mx_data.json")
        country = Country(code='MX', name='México', official_name='Estados Unidos Mexicanos')
        country.save()
        return country, data, levels

    def _make_country_data(self, country, data, levels):

        level_1 = ''
        level_2 = ''
        level_3 = ''
        level_4 = ''
        data_loaded = json.load(data)
        for item in data_loaded:
            if level_1 != item[levels['LEVEL_ONE_TYPE']]:
                level_1 = item[levels['LEVEL_ONE_TYPE']]
                levelone = LevelOne(kind=levels['LEVEL_ONE_TYPE'], name=level_1, country=country)
                levelone.save()
                # print(levelone)

            if level_2 != item[levels['LEVEL_TWO_TYPE']]:
                level_2 = item[levels['LEVEL_TWO_TYPE']]
                leveltwo = LevelTwo(kind=levels['LEVEL_TWO_TYPE'], name=level_2, levelone=levelone)
                leveltwo.save()
                # print(leveltwo)

            if len(levels) == 3:
                level_3 = item[levels['LEVEL_THREE_TYPE']]
                levelthree = LevelThree(kind=levels['LEVEL_THREE_TYPE'], name=level_3, leveltwo=leveltwo)
                levelthree.save()
                # print(levelthree)
                continue
            elif level_3 != item[levels['LEVEL_THREE_TYPE']]:
                level_3 = item[levels['LEVEL_THREE_TYPE']]
                levelthree = LevelThree(kind=levels['LEVEL_THREE_TYPE'], name=level_3, leveltwo=leveltwo)
                levelthree.save()
                # print(levelthree)

            if len(levels) == 4:
                level_4 = item[levels['LEVEL_FOUR_TYPE']]
                levelfour = LevelFour(kind=levels['LEVEL_FOUR_TYPE'], name=level_4, leveltwo=leveltwo, levelthree=levelthree)
                levelfour.save()
                # print(levelfour)



    def handle(self, *args, **options):
        print('Loading countries data...')

        print('Loading DO data')
        country, data, levels = self._rd_data()
        self._make_country_data(country, data, levels)
        print('Loading VE data')
        country, data, levels = self._ve_data()
        self._make_country_data(country, data, levels)
        print('Loading MX data')
        country, data, levels = self._mx_data()
        self._make_country_data(country, data, levels)

        print('Countries data loaded.')
