from django.db import models
from django.db.models import Q
from django.contrib.auth.models import AbstractUser

class CustomUser(AbstractUser):
    pass


class Country(models.Model):
    code = models.CharField(max_length=2)
    name = models.CharField(max_length=150)
    official_name = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.kind}: {self.name}'

class LevelOne(models.Model):
    kind = models.CharField(max_length=40)
    name = models.CharField(max_length=150)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.kind}: {self.name}'

class LevelTwo(models.Model):
    kind = models.CharField(max_length=40)
    name = models.CharField(max_length=150)
    levelone = models.ForeignKey(LevelOne, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.kind}: {self.name}'


class LevelThree(models.Model):
    kind = models.CharField(max_length=40)
    name = models.CharField(max_length=150)
    leveltwo = models.ForeignKey(LevelTwo, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.kind}: {self.name}'

# bases in a solution shown here: https://stackoverflow.com/questions/59643184/creating-a-model-with-two-optional-but-one-mandatory-foreign-key
class LevelFour(models.Model):
    kind = models.CharField(max_length=40)
    name = models.CharField(max_length=150)
    leveltwo = models.ForeignKey(LevelTwo, on_delete=models.CASCADE)
    levelthree = models.ForeignKey(LevelThree, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return f'{self.kind}: {self.name}'