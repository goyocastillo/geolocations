# Generated by Django 3.1.7 on 2021-03-08 22:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0006_auto_20210308_2053'),
    ]

    operations = [
        migrations.CreateModel(
            name='LevelFour',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(max_length=40)),
                ('name', models.CharField(max_length=150)),
                ('levelthree', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='locations.levelthree')),
                ('leveltwo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='locations.leveltwo')),
            ],
        ),
    ]
