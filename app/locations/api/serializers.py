from rest_framework import serializers

from locations.models import Country, LevelOne, LevelTwo, LevelThree, LevelFour


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = "__all__"
        read_only_fields = ("id",)


class LevelOneSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    kind = serializers.CharField(max_length=40)
    name = serializers.CharField(max_length=150)
    country = CountrySerializer(required=True)


class LevelTwoSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    kind = serializers.CharField(max_length=40)
    name = serializers.CharField(max_length=150)
    levelone = LevelOneSerializer(required=True)


class LevelThreeSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    kind = serializers.CharField(max_length=40)
    name = serializers.CharField(max_length=150)
    leveltwo = LevelTwoSerializer(required=True)


class LevelFourSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    kind = serializers.CharField(max_length=40)
    name = serializers.CharField(max_length=150)
    levelthree = LevelThreeSerializer(required=False)
        