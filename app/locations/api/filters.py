from django_filters import rest_framework as filters
from ..models import LevelOne


class LevelOneFilter(filters.FilterSet):
    name = filters.CharFilter(field_name="name", lookup_expr="icontains")
    
    class Meta:
        model = LevelOne
        fields = [
            "name"
        ]