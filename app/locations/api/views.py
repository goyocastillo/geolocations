from django_filters import rest_framework as filters

from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics
from rest_framework import status

from locations.models import (Country, LevelOne, LevelTwo, LevelThree, LevelFour,)
from .serializers import (CountrySerializer, 
                            LevelOneSerializer,
                            LevelTwoSerializer,
                            LevelThreeSerializer,
                            LevelFourSerializer,)
from .filters import LevelOneFilter


class CountryList(generics.ListAPIView):
    """
        Country list for this API.
    """
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class CountryDetail(generics.RetrieveAPIView):
    """
        Country details
    """
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class LevelOneList(generics.ListAPIView):
    """
        Here is the first level of territorial division. For example:
        in Venezuela is 'Estado', in Dominicana is 'Provincia', in 
        Colombia is 'Departamento'.

        kind: the type of territorial división as described earlier
        name: the name of that territory
        country: country id where is located this territory
    """

    queryset = LevelOne.objects.all()
    serializer_class = LevelOneSerializer
    filter_backends = (filters.DjangoFilterBackend, )
    filterset_class = LevelOneFilter
    filterset_fields = (
        "name",
    )

    def get(self, request, code, *args, **kwargs):
        queryset = self.filter_queryset(LevelOne.objects.filter(country__code=code))
        serializer = LevelOneSerializer(queryset, many=True)
        return Response(serializer.data)


class LevelOneDetail(generics.RetrieveAPIView):
    """
        Detail of a levelone data
    """

    queryset = LevelOne.objects.all()
    serializer_class = LevelOneSerializer

class LevelTwoList(generics.ListAPIView):
    """
        Here is the second level of territorial division. For example:
        in Venezuela is 'Municipio', in other cuntries are 'Comarca'.

        kind: the type of territorial división as described earlier
        name: the name of that territory
        levelone: where this territory belongs to
    """

    queryset = LevelTwo.objects.all()
    serializer_class = LevelTwoSerializer

    def get(self, request, pk, *args, **kwargs):
        queryset = LevelTwo.objects.filter(levelone__id=pk)
        serializer = LevelTwoSerializer(queryset, many=True)
        return Response(serializer.data)

class LevelTwoDetail(generics.RetrieveAPIView):
    """
        Detail of a leveltwo data
    """
    queryset = LevelTwo.objects.all()
    serializer_class = LevelTwoSerializer


class LevelThreeList(generics.ListAPIView):
    """
        Here is the third level of territorial division. It correspond
        to the cities in this territory. 'Ciudad' is a generic name to
        describe any place where there is people living, ie: 'Ciudad',
        'Pueblo', 'Caserío'

        kind: city as described earlier
        name: the name of that territory
        leveltwo: where this city belongs to
    """

    queryset = LevelThree.objects.all()
    serializer_class = LevelThreeSerializer

    def get(self, request, pk, *args, **kwargs):
        queryset = LevelThree.objects.filter(leveltwo__id=pk)
        serializer = LevelThreeSerializer(queryset, many=True)
        return Response(serializer.data)
class LevelThreeDetail(generics.RetrieveAPIView):
    """
        Detail of a city data
    """

    queryset = LevelThree.objects.all()
    serializer_class = LevelThreeSerializer


class LevelFourList(generics.ListAPIView):
    """
        Some countries has cities with internal divisions. In the case
        of Venezuela these are named 'Parroquia'. It is posible to have
        more detail information, for example: 'Urbanización' (Venezuela, 
        República Dominicana). 

        For reporting purposes this level is not taken into account.

        kind: sub division of the city as described earlier
        name: the name of that territory
        leveltwo: where this city belongs to
    """
    queryset = LevelFour.objects.all()
    serializer_class = LevelFourSerializer

    def get(self, request, pk, *args, **kwargs):
        queryset = LevelFour.objects.filter(levelthree__id=pk)
        serializer = LevelFourSerializer(queryset, many=True)
        return Response(serializer.data)


class LevelFourDetail(generics.RetrieveAPIView):
    """
        Detail of a levefour data
    """
    queryset = LevelFour.objects.all()
    serializer_class = LevelFourSerializer
