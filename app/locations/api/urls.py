from django.urls import path

from . import views

urlpatterns = [
    path("locations/api/countries/", views.CountryList.as_view(), name="country-list"),
    path("locations/api/countries/<int:pk>/", views.CountryDetail.as_view(), name="country-detail"),
    path("locations/api/countries/<str:code>/levelones/", views.LevelOneList.as_view(), name='levelone-list'),
    path("locations/api/levelones/<int:pk>/", views.LevelOneDetail.as_view(), name='levelone-detail'),
    path("locations/api/levelones/<int:pk>/leveltwos/", views.LevelTwoList.as_view(), name='leveltwo-list'),
    path("locations/api/leveltwos/<int:pk>/", views.LevelTwoDetail.as_view(), name='leveltwo-detail'),
    path("locations/api/leveltwos/<int:pk>/levelthrees/", views.LevelThreeList.as_view(), name='levelthree-list'),
    path("locations/api/levelthrees/<int:pk>/", views.LevelThreeDetail.as_view(), name='levelthree-detail'),
    path("locations/api/levelthrees/<int:pk>/levelfours/", views.LevelFourList.as_view(), name='levelfour-list'),
    path("locations/api/levelfours/<int:pk>/", views.LevelFourDetail.as_view(), name='levelfour-detail'),
]