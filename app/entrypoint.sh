#!/bin/sh

if [ -z "${SQL_USER}" ]; then
    base_postgres_image_default_user='postgres'
    export SQL_USER="${base_postgres_image_default_user}"
fi

# Database 
postgres_ready() {
python << END
import sys

import psycopg2

try:
    psycopg2.connect(
        dbname="${SQL_DATABASE}",
        user="${SQL_USER}",
        password="${SQL_PASSWORD}",
        host="${SQL_HOST}",
        port="${SQL_PORT}",
    )
except psycopg2.OperationalError:
    sys.exit(-1)
sys.exit(0)

END
}
until postgres_ready; do
  >&2 echo 'Waiting for PostgreSQL to become available...'
  sleep 1
done
>&2 echo 'PostgreSQL is started'

python manage.py flush --no-input
python manage.py migrate

python manage.py load_data
# python manage.py load_rd
# python manage.py load_vzla

exec "$@"